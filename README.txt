CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
-------------
The Waywire field module provides a simple field that allows you to add a
Waywire video to a content type, user, or any entity.

Display types include:

 * Waywire videos of various sizes and options.
 * Waywire thumbnails with image styles.


REQUIREMENTS
------------
This module requires the following modules:
 * Field (core)
 * File (core)
 * Image (core)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
* Once module is installed make sure to fill out the configration page which
  can be found at: Home » Administration » Configuration » Media


MAINTAINERS
-----------
Current maintainers:
 * Albert Jankowski (albertski) - https://www.drupal.org/user/1327432
